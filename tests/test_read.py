﻿import os
# License notice
#  
# Copyright 2017
# 
# Energistics 
# The following Energistics (c) products were used in the creation of this work: 
# 
# •             PRODML Data Schema Specifications, Version 1.3.1 
# •             PRODML Data Schema Specifications, Version 2.0 
# 
# All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
# any portion thereof, which remain in the Standards DevKit shall remain with Energistics
# or its suppliers and shall remain subject to the terms of the Product License Agreement
# available at http://www.energistics.org/product-license-agreement. 
# 
# Apache
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
# except in compliance with the License. 
# 
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software distributed under the
# License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied. 
# 
# See the License for the specific language governing permissions and limitations under the
# License.
# 
# All rights reserved. 
# 
#

import sys; sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))

import numpy as np
import pytest

from prodml_das.prodml import PMLproxy

def test_read_metadata():
    pml_object = PMLproxy(os.path.join(os.path.dirname(__file__), 'data/testcase/case1.epc'))
    assert pml_object.das_acquisition != None
    assert pml_object.das_instrument_box != None
    assert pml_object.fiber_optical_path != None
    dasdata, timestamps = pml_object.read_raw_traces(pml_object.das_acquisition.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0],
                                                     pml_object.das_acquisition.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0],
                                                     0, pml_object.das_acquisition.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].Count//pml_object.das_acquisition.Raw[0].NumberOfLoci)
    np.testing.assert_array_equal(dasdata, np.ones(dasdata.shape, dtype=dasdata.dtype))
    np.testing.assert_array_equal(timestamps, np.ones(timestamps.shape, dtype=dasdata.dtype))
    dasdata, timestamps = pml_object.read_raw_traces(pml_object.das_acquisition.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[1],
                                                     pml_object.das_acquisition.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[1],
                                                     0, pml_object.das_acquisition.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[1].Count//pml_object.das_acquisition.Raw[0].NumberOfLoci)
    np.testing.assert_array_equal(dasdata, np.zeros(dasdata.shape, dtype=dasdata.dtype))
    np.testing.assert_array_equal(timestamps, np.zeros(timestamps.shape, dtype=dasdata.dtype))
    trigger_time = pml_object.read_raw_trigger_time(pml_object.das_acquisition.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0])
    assert trigger_time == 2

def test_case_1():
    pml_object = PMLproxy(os.path.join(os.path.dirname(__file__), 'data/Case1/Case1.epc'))
    assert pml_object.das_acquisition

def test_case_2():
    pml_object = PMLproxy(os.path.join(os.path.dirname(__file__), 'data/Case2/Case2.epc'))
    assert pml_object.das_acquisition

def test_case_3():
    pml_object = PMLproxy(os.path.join(os.path.dirname(__file__), 'data/Case3/Case3.epc'))
    assert pml_object.das_acquisition

def test_case_4():
    pml_object = PMLproxy(os.path.join(os.path.dirname(__file__), 'data/Case4/Case4.epc'))
    assert pml_object.das_acquisition

def test_case_5():
    pml_object = PMLproxy(os.path.join(os.path.dirname(__file__), 'data/Case5/Case5.epc'))
    assert pml_object.das_acquisition

def test_case_6():
    pml_object = PMLproxy(os.path.join(os.path.dirname(__file__), 'data/Case6/Case6.epc'))
    assert pml_object.das_acquisition
    fbe_file_proxy = pml_object.das_acquisition.Processed.Fbe[0].FbeData[0].FbeDataArray.Values.ExternalFileProxy[0]
    assert fbe_file_proxy.Count == 450
    fbe_data = pml_object.read_fbe_data(fbe_file_proxy, 5, 1)
    assert fbe_data[0, 0] == pytest.approx(0.0)
    assert fbe_data[0, 1] == pytest.approx(0.009077863)

def test_case_example():
    pml_object = PMLproxy(os.path.join(os.path.dirname(__file__), 'data/example/example.epc'))
    assert pml_object.das_acquisition
